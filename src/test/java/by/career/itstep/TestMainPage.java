package by.career.itstep;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.testng.Assert.assertTrue;
import static org.testng.AssertJUnit.assertFalse;

public class TestMainPage {

    WebDriver driver;
    MainPage page;

    @BeforeClass
    public void setUp () {
        System.setProperty("webdriver.chrome.driver", "src/test/resources/chromedriver.exe");
        driver = new ChromeDriver();
        page = new MainPage(driver);
    }

    @AfterClass
    public void shutDown () {
        driver.close();
    }

    @Test
    public void test () {
        page.open();

        assertFalse(page.isConsultationModalOpen());

        assertTrue(page.homePage());
    }

    @Test
    public void testOpenConsultationModal () throws Exception {
        page.open();
        page.openConsultationModal();

        Thread.sleep(1000);  //wait 1s

        assertTrue(page.isConsultationModalOpen());
    }

    @Test
    public void testFillConsultationForm () throws Exception {
        page.open();
        page.openConsultationModal();
        Thread.sleep(1000);  //wait 1s
        page.fillConsulationForm("Name", "LastName", "Gurmanmaks@mail.ru", "+375295650750",true, "QA3819");
    }

    @Test
    public void testOPenEventsPage_happyPath () throws Exception {
        page.open();
        page.openEventsPage();

        Thread.sleep(1000);

        assertTrue(page.eventPage());
    }

    @Test
    public void testOpenProjectPage_happyPath () throws Exception {
        page.open();
        page.openProjectPage();

        Thread.sleep(1000);

        assertTrue(page.projectPAge());
    }

    @Test
    public void testOpenStoriesPage_happyPath () throws Exception {
        page.open();
        page.openStoriesPage();

        Thread.sleep(1000);

        assertTrue(page.storiesPage());
    }

    @Test
    public void testOpenVacanciesPage_happyPath () throws Exception {
        page.open();
        page.openVacanciesPage();

        Thread.sleep(1000);

        assertTrue(page.vacanciesPage());
    }

    @Test
    public void testOpenPartnersPage_happyPAth () throws Exception {
        page.open();
        page.openPartnersPage();

        Thread.sleep(1000);

        assertTrue(page.partnersPage());
    }
}
