package by.career.itstep;

import io.restassured.RestAssured;
import io.restassured.specification.RequestSpecification;
import org.apache.http.HttpStatus;
import org.json.simple.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;
import io.restassured.response.Response;

import static org.apache.http.HttpStatus.SC_BAD_REQUEST;
import static org.apache.http.HttpStatus.SC_CREATED;


public class TestVacancyApi {

    private static final String EXP_Intern = "Intern";
    private static final String EXP_Junior = "JUNIOR";
    private static final String EXP_Middle = "Middle";
    private static final String EXP_Senior = "Senior";

    private static final String Base_Url = "http://13.48.31.160:8080/api/v1";

    @Test
    public void testCreateVacancy_happyPath () {
        RestAssured.baseURI = Base_Url;
        String path = "/vacancies";

        RequestSpecification req = RestAssured.given();

        JSONObject json = new JSONObject();
        json.put("company", "IT-Step");
        json.put("description", "Cool job description");
        json.put("experience",EXP_Junior);
        json.put("name", "developer");

        req.header("Content-Type", "application/json");
        req.body(json.toJSONString()); // add JSON at String

        Response response = req.request(io.restassured.http.Method.POST, path);
        String body = response.getBody().asString();
        int code = response.getStatusCode();

        System.out.println("Body: " + body);
        System.out.println("Code: " + code);

        Assert.assertEquals(code, SC_CREATED);
    }

    @Test
    public void testCreateVacancyIfNotValid () {
        RestAssured.baseURI = Base_Url;
        String path = "/vacancies";

        RequestSpecification req = RestAssured.given();

        JSONObject json = new JSONObject();
        json.put("company", "");
        json.put("description", "Cool job description");
        json.put("experience",EXP_Junior);
        json.put("name", "developer");

        req.header("Content-Type", "application/json");
        req.body(json.toJSONString()); // add JSON at String

        Response response = req.request(io.restassured.http.Method.POST, path);
        String body = response.getBody().asString();
        int code = response.getStatusCode();

        System.out.println("Body: " + body);
        System.out.println("Code: " + code);

        Assert.assertEquals(code, SC_BAD_REQUEST);
    }

    @Test
    public void testFindVacancyById () {
        String path = "/vacancies";
        String id = "e886224f-370f-4ab7-969b-a653cc525535";
        Response response = RestAssured.given().when().get(path); // = req.request(Method.GET, path);
    }
}
