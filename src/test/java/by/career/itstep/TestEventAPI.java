package by.career.itstep;

import io.restassured.RestAssured;
import io.restassured.http.Method;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.apache.http.HttpStatus;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.UUID;

import static org.apache.http.HttpStatus.SC_NOT_FOUND;
import static org.apache.http.HttpStatus.SC_OK;

public class TestEventAPI {

    private static final String Base_Url = "http://13.48.31.160:8080/api/v1";
    private static final String Status_PUBLISHED = "PUBLISHED";

    @Test
    public void testFindAllEvents_happyPath () {
        int page = 0;
        int size = 3;

        RestAssured.baseURI = Base_Url;
        String path = String.format("events?page=%s&size=%s", page, size);

        RequestSpecification req = RestAssured.given();
        Response response = req.request(Method.GET, path);

        String jsonBody = response.getBody().asString();
        int code = response.getStatusCode();

        System.out.println("Body: " + jsonBody);
        System.out.println("Code: " + code);

        Assert.assertEquals(SC_OK, code);
    }

    @Test
    public void testFindEventByID_happyPath () {
        String id = "1469a0b5-6640-4184-a136-05a09aa9d719";

        RestAssured.baseURI = Base_Url;
        String path = String.format("events/%s", id);

        RequestSpecification req = RestAssured.given();
        Response response = req.request(Method.GET, path);

        String jsonBody = response.getBody().asString();
        int code = response.getStatusCode();
        String status = response.getBody().jsonPath().get("status");

        System.out.println("Body: " + jsonBody);
        System.out.println("Code: " + code);
        System.out.println("Status: " + status);

        Assert.assertEquals(code, SC_OK);
        Assert.assertEquals(status, Status_PUBLISHED);
    }

    @Test
    public void testFindEventByIdNotExists() {
        String wrongId = UUID.randomUUID().toString();

        RestAssured.baseURI = Base_Url;
        String path = String.format("events/%s", wrongId);

        RequestSpecification req = RestAssured.given();
        Response response = req.request(Method.GET, path);

        String jsonBody = response.getBody().asString();
        int code = response.getStatusCode();

        System.out.println("Body: " + jsonBody);
        System.out.println("Code: " + code);

        Assert.assertEquals(code, SC_NOT_FOUND);
    }
}
