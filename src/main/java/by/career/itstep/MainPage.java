package by.career.itstep;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import static by.career.itstep.util.WaitUtil.waitUntilVisible;


public class MainPage {

    private WebDriver driver;
    private static final String URL = "http://13.48.31.160:8080/index";
    private static final String URLevent = "http://13.48.31.160:8080/events?page=1";
    private static final String URLproject = "http://13.48.31.160:8080/projects?page=1";
    private static final String URLstories = "http://13.48.31.160:8080/stories?page=1";
    private static final String URLvacancies = "http://13.48.31.160:8080/vacancies?page=1";
    private static final String URLpartners = "http://13.48.31.160:8080/partners";

    private static final By Field_Name = By.xpath("//input[@id='name']");
    private static final By Field_SurName = By.xpath("//input[@id='surname']");
    private static final By Field_Email = By.xpath("//input[@id='email']");
    private static final By Field_Tel = By.xpath("//input[@id='tel']");
    private static final By Checkbox_studentStep = By.xpath("//label[@for='studentStep']");
    private static final By Field_NumberGroup = By.xpath("//input[@id='numberGroup']");
    private static final By Select_StateDay = By.xpath("//select[@id='inputStateDay']");
    private static final By Select_StateTime = By.xpath("//select[@id='inputStateTime']");


    public MainPage (WebDriver driver) {
        this.driver = driver;
    }

    public void open () {
        driver.get(URL);
    }

    public boolean homePage () {
        return driver.getCurrentUrl().equals(URL);
    }

    public boolean eventPage () {
        return driver.getCurrentUrl().equals(URLevent);
    }

    public boolean projectPAge () {
        return driver.getCurrentUrl().equals(URLproject);
    }

    public boolean storiesPage () {
        return driver.getCurrentUrl().equals(URLstories);
    }

    public boolean vacanciesPage () {
        return driver.getCurrentUrl().equals(URLvacancies);
    }

    public boolean partnersPage () {
        return driver.getCurrentUrl().equals(URLpartners);
    }

    public boolean isConsultationModalOpen () {
//        WebElement modal = waitUntilVisible(driver, By.xpath("//div[@id='modalConsult']"), 10);

        WebElement modal = driver.findElement(By.xpath("//div[@id='modalConsult']"));

        String classes = modal.getAttribute("class");
        return classes.contains("show");
    }

    public void openConsultationModal () {
        WebElement link = driver.findElement(By.xpath("//ul[@id='myMenu']/li/a[contains(text(),'Консультация')]"));
        link.click();
    }

    public void fillConsulationForm (String name, String lastName, String email, String phone, boolean isStudent, String group) throws Exception {
        driver.findElement(Field_Name).sendKeys(name);

        driver.findElement(Field_SurName).sendKeys(lastName);

        driver.findElement(Field_Email).sendKeys(email);

        driver.findElement(Field_Tel).sendKeys(phone);

        if (isStudent){
            driver.findElement(Checkbox_studentStep).click();
            Thread.sleep(1000);
            driver.findElement(Field_NumberGroup).sendKeys(group);
        }

        WebElement elementDay = driver.findElement(Select_StateDay);
        Select selectDay = new Select(elementDay);
        selectDay.selectByIndex(2);

        Thread.sleep(1000);

        WebElement elementTime = driver.findElement(Select_StateTime);
        Select selectTime = new Select(elementTime);
        selectTime.selectByIndex(2);
    }

    public void openEventsPage () {
        WebElement link = driver.findElement(By.xpath("//ul[@id='myMenu']/li/a[contains(text(),'Мероприятия')]"));
        link.click();
    }

    public void openProjectPage () {
        WebElement link = driver.findElement(By.xpath("//ul[@id='myMenu']/li/a[contains(text(),'Проекты')]"));
        link.click();
    }

    public void openStoriesPage () {
        WebElement link = driver.findElement(By.xpath("//ul[@id='myMenu']/li/a[contains(text(),'Истории')]"));
        link.click();
    }

    public void openVacanciesPage () {
        WebElement link = driver.findElement(By.xpath("//ul[@id='myMenu']/li/a[contains(text(),'Вакансии')]"));
        link.click();
    }

    public void openPartnersPage () {
        WebElement link = driver.findElement(By.xpath("//ul[@id='myMenu']/li/a[contains(text(),'Партнеры')]"));
        link.click();
    }
}
